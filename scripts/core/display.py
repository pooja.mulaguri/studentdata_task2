import json


class Display:
    @staticmethod
    def display_jsondata(dictionary_obtained):
        jsondata = json.dumps(dictionary_obtained)
        return jsondata
    @staticmethod
    def display_pythondata(json_dictionary):
        pythondata = json.loads(json_dictionary)
        return pythondata

