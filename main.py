from scripts.core.display import Display

object1 = Display()
# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    dictionary_format = dict()
    name = input("enter name")
    dictionary_format["name"] = name
    department = input("enter department name")
    dictionary_format["department"] = department
    class_n = input("enter class name")
    dictionary_format["class"] = class_n
    college_name = input("enter college")
    dictionary_format["college_name"] = college_name
    print("enter number of courses")
    number_of_courses = int(input())
    list1 = []
    for i in range(0, number_of_courses):
        dict1 = dict()
        k = "course" + str(i + 1)
        course_name = input("enter " + str(k))
        course_code = input("enter course_code of " + str(k))
        dict1["course_name"] = course_name
        dict1["course_code"] = course_code
        print(dict1)
        list1.append(dict1)
    dictionary_format["courses"] = list1
    jsondata_obtained = object1.display_jsondata(dictionary_format)
    print("json data",jsondata_obtained)
    print("python data",object1.display_pythondata(jsondata_obtained))
